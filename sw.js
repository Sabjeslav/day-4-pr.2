self.addEventListener('install', function(event) {
    console.log('[Service Worker] Installing Service Worker ...', event);
    event.waitUntil(
        caches.open("static")
            .then(function (cache) {
                console.log("precaching");
                cache.add('/index.html');
                cache.add("/css/bootstrap.css");
                cache.add("/css/style.css");
                cache.add("/css/order.css");
                cache.add("/css/jquery-ui.css");
                cache.add("/css/specs.css");
                cache.add("/images/logotip.png");
                cache.add("/images/logo.png");
                cache.add('/');
            })
    );
});
self.addEventListener('activate', function(event) {
    console.log('[Service Worker] Activating Service Worker ...', event);
    return self.clients.claim();
});
self.addEventListener('fetch', function(event) {
    console.log('[Service Worker] Fetching something ....', event);
    event.respondWith(
    caches.match(event.request)
        .then(function (response) {
            if (response)
                return response;
            else
                return  fetch(event.request);
        })
);
});

self.addEventListener('push', event =>{
    const notification=event.data.text();
    self.registration.showNotification(notification, {})
});